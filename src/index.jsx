import ForgeUI, { render, Fragment, IssuePanel, useProductContext, useState, Image, Button } from "@forge/ui";
import api from "@forge/api";

const countWords = require('./countWords');

let wordData = [];

const filterWord = ["the", "and", "a", "to"];

const state = {
  refresh: true
}

const createWordCloud = () => {
  const properties = {
    svgWidth: 600,
    svgHeight: 200,
    wordObjects: [...wordData]
  }

  let groupArray = [];

  properties.wordObjects.map((item) => {
    item.x = createXY(properties.svgWidth, 'x');
    item.y = createXY(properties.svgHeight, 'y');
    item.opacity = createOpacity(item.occurences, properties.wordObjects);
    item.fontSize = createFontSize(item.occurences, properties.wordObjects);

    item = adjustPlacement(properties.svgWidth, properties.svgHeight, item);
    groupArray.push(generateSVGGroup(item.x, item.y, item.opacity, item.fontSize, item.word, false));
  });

  const finalSVG = 'data:image/svg+xml;utf8,' + `<svg xmlns="http://www.w3.org/2000/svg" width="` + properties.svgWidth + `" height="` + properties.svgHeight + `">` + groupArray.join('') + `</svg>`

  return finalSVG;
}

const adjustPlacement = (maxWidth, maxHeight, item) => {

  if (item.y < item.fontSize) {
    item.y = generateRandomNumber(item.fontSize, maxHeight);
  }

  if (item.x + getWordWidth(item.word, item.fontSize) > maxWidth) {
    item.x = generateRandomNumber(0, maxWidth - getWordWidth(item.word, item.fontSize));
  }

  return item;
}

const createFontSize = (occurences, obj) => {
  const minFontSize = 10;
  const maxFontSize = 50;
  return Math.round((occurences / getMostOccurred(obj)) * maxFontSize + minFontSize);
}

const createOpacity = (occurences, obj) => {
  return occurences / getMostOccurred(obj);
}

const createXY = (value, type) => {
  let number = generateRandomNumber(0, value);
  return number;
}

const getMostOccurred = (obj) => {
  let most = 0;
  obj.map((p) => {
    if (p.occurences > most) {
      most = p.occurences;
    }
  });
  return most;
}

const getWordWidth = (word, fontSize) => {
  return word.length * (fontSize / 2)
}

const generateRandomNumber = (min, max) => {
  return Math.floor((Math.random() * (max - min)) + min);
}

const generateSVGGroup = (x, y, opacity, fontSize, word, animation) => {
  return `
  <g>`+
    `<text x='` + x + `' y='` + y + `' font-family="Arial" opacity='` + opacity + `' font-size='` + fontSize + `'>` + word + `</text>
  </g>`;
}

const searchAndRemoveCommon = (arr) => {
  const result = arr.filter((obj) => isAcceptableWord(obj.word));
  return result;
}

const isAcceptableWord = (word) => {
  let acceptable = true;
  filterWord.map((w) => {
    if (w == word) {
      acceptable = false;
    }
  });

  return acceptable;
}

const App = () => {
  const context = useProductContext();
  const [everything] = useState(async () => await fetchIssue(context.platformContext.issueKey));
  wordData = searchAndRemoveCommon(countWords.countWords(everything["fields"])).slice(0, 20);
  const [refresh, setRefresh] = useState(state.refresh);

  return (
    <Fragment>
      <Image src={createWordCloud()} alt="Word Cloud"></Image>
      <Button text="Re-generate" onClick={() => { setRefresh(!refresh) }} />
    </Fragment>
  );

};

const fetchIssue = async (issueId) => {
  const res = await api
    .asApp()
    .requestJira(`/rest/api/3/issue/${issueId}`);

  const data = await res.json();
  return data;
};

export const run = render(
  <IssuePanel>
    <App />
  </IssuePanel>
);
