
/*
import file using: 
const countWords = require('./countWords');

call function like :
countWords.countWords(everything["fields"])
*/


// PASS "everything["fields"] to this function please
const countWords = (jsonData) => {
    var arr = getTextArray(jsonData);
    var hash = {};
    for (var i = 0; i < arr.length; ++i) {
        var wordsArr = cleanTextAndGetWordsArray(arr[i]);
        for (var j = 0; j < wordsArr.length; ++j) {
            var currWord = wordsArr[j];
            if (!(currWord in hash)) {
                hash[currWord] = 0;
            }
            hash[currWord]++;
        }
    }
    var unsorted = createJSON(hash);

    var toReturn = unsorted.sort(function (a, b) {
        return parseInt(b['occurences']) - parseInt(a['occurences']);
    });


    return toReturn;
}


const createJSON = (hash) => {
    var json = [];
    for (key in hash) {
        json.push({
            "word": key,
            "occurences": hash[key]
        })
    }
    return json
}

const cleanTextAndGetWordsArray = (text) => {
    var str = text.replace(/[.,\/#!$%\^&\*;:{}=\-_`~()]/g, "");
    str = str.replace(/\s{2,}/g, " ");
    str = str.replace(/\s\s+/g, ' ');
    str = str.toLowerCase();
    var wordsArr = str.split(" ");

    return wordsArr;
    // remove common words like "is, it the etc."
    // should we capitalise it?
}



const getTextArray = (jsonData) => {
    var arr = [];
    const summary = jsonData["summary"];
    if (summary != null && typeof summary == "string") arr.push(jsonData["summary"]);

    const [_, description] = findValueByKey(jsonData["description"], "text") ? findValueByKey(jsonData["description"], "text") : '';
    if (description != null && typeof description == "string") arr.push(description);

    const comments = jsonData["comment"]["comments"];
    Object.keys(comments).forEach(function (key) {
        var [_, value] = findValueByKey(comments[key], "text");
        if (value != null && typeof value == "string") {
            arr.push(value);
        }
    });
    return arr;
}


const findValueByKey = (jasnData, myKey) => {
    for (key in jasnData) {
        value = jasnData[key];
        if (myKey == key) return [myKey, value];
        if (typeof value == "object") {
            var y = findValueByKey(value, myKey);
            if (y && y[0] == myKey) return y;
        }
        if (typeof value == "array") {
            for (var i = 0; i < value.length; ++i) {
                var x = findValueByKey(value[i], myKey);
                if (x && x[0] == myKey) return x;
            }
        }
    }
    return null;
}

exports.countWords = countWords;