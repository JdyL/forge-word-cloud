# Forge Word Cloud

This project contains a Forge app written in Javascript that displays a Word Cloud in a Jira issue panel. 

![Word Cloud Gif](./docs/word-cloud.gif)

## Requirements

Word Cloud requires read permission in JIRA.

See [Set up Forge](https://developer.atlassian.com/platform/forge/set-up-forge/) for instructions to get set up.

## Installation

Follow the guide here: https://developer.atlassian.com/platform/forge/example-apps/ for installing and testing the Forge app locally.

## Documentation
The app reads the current issue panel and displays the words in a SVG format. 
The words will displayed calculated opacity and font size based on its occurrence amount.

* In order to prevent a whole lot of words, there is a limit to the top 20 most occurred words.
